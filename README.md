Project skeleton
==================

This is a pseudo-project meant to be exported rather than checked out,
in order to create a consistent new project layout. It also comes with
some useful Rake tasks for building a Deb and deploying.

To start from it, first create a new project directory. Then cd into
that directory and type:

    curl https://bitbucket.org/bnac/project_skeleton/get/master.tar.gz | tar -zx -C . --strip 1

Then proceed as usual. As this is not a clone, it won't be git-enabled
yet.
